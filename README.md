# WebRTC Universial Binary

This is WebRTC framework in XCFramework format for iOS (arm and x64).

- https://webrtc.googlesource.com/src/+log/refs/branch-heads/4240

## Installation

### Swift Package Manager 

```swift
dependencies: [
    .package(url: "https://bitbucket.org/projectorinc/sm-webrtc-ios.git", .upToNextMajor(from: "86.4240.0"))
]
```

### CocoaPods

```swift
pod 'sm-webrtc-ios', '~> 86.4240'
```
